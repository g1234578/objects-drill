function mapObjects(obj, cb) {
  let mappedObject = {};

  if(typeof(obj) === 'object'){
    for (let key in obj) {
      mappedObject[key] = cb(key,obj[key]);
    }
    return mappedObject;
  }

  else{
    return [];
  }
}

module.exports = mapObjects;