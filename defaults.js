function defaults(obj, defaultProps) {
  if (typeof obj === "object" && typeof defaultProps === "object") {
    for (let key in defaultProps) {
      if (!(key in obj)) {
        obj[key] = defaultProps[key];
      }
    }
    return obj;
  }
  else{
    return [];
  }
}

module.exports = defaults;
