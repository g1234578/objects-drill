function invert(obj) {
  let invertedObject = {};

  if (typeof obj === "object") {
    for (let key in obj) {
      invertedObject[obj[key]] = key;
    }
    return invertedObject;
  }
}

module.exports = invert;
