function values(obj) {
  let valuesArray = [];

  if (typeof obj == "object") {
    for (let key in obj) {
      let value = obj[key];
      if (typeof value !== "function") {
        valuesArray.push(obj[key]);
      }
    }
    return valuesArray;
  }
}

module.exports = values;
