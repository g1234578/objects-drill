function keys(obj) {
  let keyArray = [];

  if (typeof obj === "object") {
    for (let key in obj) {
      keyArray.push(key);
    }
    return keyArray;
  }
}

module.exports = keys;
