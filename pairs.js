function pairs(obj) {
  let pairsArray = [];
  if(typeof(obj)){
    for (let key in obj) {
      pairsArray.push([key, obj[key]]);
    }
    return pairsArray;
  }

  else{
    return [];
  }
}

module.exports = pairs;
